﻿using System;
using UnityEngine;


[Serializable]
public class Tools {

    public int id;
    public string name;

    public GameObject head;

    public ToolType toolType;
    public enum  ToolType { Brush, Splatter }

    public SplatterType splatterType;
    public enum SplatterType { Spray, Stamp }

    public Texture2D texture;
    public Sprite icon;
}
